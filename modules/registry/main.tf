# Copyright 2022 Taavi Väänänen <hi@taavi.wtf>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

terraform {
  required_version = ">= 1.4.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.51.1"
    }

    cloudvps = {
      source  = "terraform.wmcloud.org/registry/cloudvps"
      version = "~> 0.2.0"
    }
  }
}

variable "registry_data_volume_size" {
  type = number
}

variable "registry_vm_generation" {
  type = number
}

variable "registry_vm_image_name" {
  type = string
}

variable "registry_vm_flavor_name" {
  type = string
}

variable "registry_proxy_hostname" {
  type = string
}

variable "registry_proxy_domain" {
  type = string
}

resource "openstack_blockstorage_volume_v3" "registry_data" {
  name        = "tf-registry-data"
  size        = var.registry_data_volume_size
  volume_type = "standard"
}

data "openstack_compute_flavor_v2" "registry_vm_flavor" {
  name = var.registry_vm_flavor_name
}

data "openstack_images_image_v2" "registry_vm_image" {
  most_recent = true
  name        = var.registry_vm_image_name
}

data "openstack_networking_network_v2" "lan_flat_cloudinstances2b" {
  name = "lan-flat-cloudinstances2b"
}

data "openstack_networking_secgroup_v2" "default" {
  name = "default"
}

resource "openstack_networking_secgroup_v2" "registry_proxy_access" {
  name = "registry-proxy-access"
}

resource "openstack_networking_secgroup_rule_v2" "registry_proxy_access_http" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "172.16.0.0/21"
  security_group_id = openstack_networking_secgroup_v2.registry_proxy_access.id
}

resource "openstack_compute_instance_v2" "registry_vm" {
  name      = "tf-registry-${var.registry_vm_generation}"
  image_id  = data.openstack_images_image_v2.registry_vm_image.id
  flavor_id = data.openstack_compute_flavor_v2.registry_vm_flavor.id

  security_groups = [
    data.openstack_networking_secgroup_v2.default.name,
    openstack_networking_secgroup_v2.registry_proxy_access.name,
  ]

  network {
    uuid = data.openstack_networking_network_v2.lan_flat_cloudinstances2b.id
  }

  lifecycle {
    ignore_changes = [
      image_id,
    ]
  }
}

resource "openstack_compute_volume_attach_v2" "registry_vm_data" {
  instance_id = openstack_compute_instance_v2.registry_vm.id
  volume_id   = openstack_blockstorage_volume_v3.registry_data.id
}

resource "cloudvps_web_proxy" "registry_proxy" {
  hostname = var.registry_proxy_hostname
  domain   = var.registry_proxy_domain
  backends = ["http://${openstack_compute_instance_v2.registry_vm.access_ip_v4}:80"]
}
