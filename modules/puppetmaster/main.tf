# Copyright 2022 Taavi Väänänen <hi@taavi.wtf>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

terraform {
  required_version = ">= 1.4.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.51.1"
    }
  }
}

variable "puppetmaster_vm_generation" {
  type = number
}

variable "puppetmaster_vm_image_name" {
  type = string
}

variable "puppetmaster_vm_flavor_name" {
  type = string
}

data "openstack_compute_flavor_v2" "puppetmaster_vm_flavor" {
  name = var.puppetmaster_vm_flavor_name
}

data "openstack_images_image_v2" "puppetmaster_vm_image" {
  most_recent = true
  name        = var.puppetmaster_vm_image_name
}

data "openstack_networking_network_v2" "lan_flat_cloudinstances2b" {
  name = "lan-flat-cloudinstances2b"
}

data "openstack_networking_secgroup_v2" "default" {
  name = "default"
}

resource "openstack_compute_instance_v2" "puppetmaster_vm" {
  name      = "tf-pm-${var.puppetmaster_vm_generation}"
  image_id  = data.openstack_images_image_v2.puppetmaster_vm_image.id
  flavor_id = data.openstack_compute_flavor_v2.puppetmaster_vm_flavor.id

  security_groups = [
    data.openstack_networking_secgroup_v2.default.name,
  ]

  network {
    uuid = data.openstack_networking_network_v2.lan_flat_cloudinstances2b.id
  }

  lifecycle {
    ignore_changes = [
      image_id,
    ]
  }
}
