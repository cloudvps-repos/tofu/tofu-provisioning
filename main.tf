# Copyright 2022 Taavi Väänänen <hi@taavi.wtf>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

terraform {
  required_version = ">= 1.4.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.51.1"
    }

    cloudvps = {
      source  = "terraform.wmcloud.org/registry/cloudvps"
      version = "~> 0.2.0"
    }
  }
}

variable "os_auth_url" { type = string }
variable "os_project_id" { type = string }
variable "os_application_credential_id" { type = string }
variable "os_application_credential_secret" {
  type      = string
  sensitive = true
}

provider "openstack" {
  auth_url                      = var.os_auth_url
  tenant_id                     = var.os_project_id
  application_credential_id     = var.os_application_credential_id
  application_credential_secret = var.os_application_credential_secret
}

provider "cloudvps" {
  os_auth_url                      = var.os_auth_url
  os_project_id                    = var.os_project_id
  os_application_credential_id     = var.os_application_credential_id
  os_application_credential_secret = var.os_application_credential_secret
}

module "puppetmaster" {
  source = "./modules/puppetmaster"

  puppetmaster_vm_generation  = 1
  puppetmaster_vm_image_name  = "debian-11.0-bullseye"
  puppetmaster_vm_flavor_name = "g4.cores1.ram2.disk20"
}

module "registry" {
  source = "./modules/registry"

  registry_data_volume_size = 5

  registry_vm_generation  = 2
  registry_vm_image_name  = "debian-11.0-bullseye"
  registry_vm_flavor_name = "g4.cores1.ram2.disk20"

  registry_proxy_hostname = "terraform"
  registry_proxy_domain   = "wmcloud.org"
}
